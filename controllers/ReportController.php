<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ReportForm;

class ReportController extends Controller
{

    /**
     * Displays report page.
     *
     * @return Response|string
     */
    public function actionIndex()
    {
        $model = new ReportForm();
        if ($model->load(Yii::$app->request->post())) {
            //Yii::$app->session->setFlash('contactFormSubmitted');

            //return $this->refresh();
        }
        return $this->render('index', [
            'model' => $model,
            'data' => $model->calculate(),
        ]);
    }

}

<?php

namespace app\controllers;

use app\models\Author;
use app\models\AuthorSearch;
use app\models\UserAuthor;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AuthorController implements the CRUD actions for Author model.
 */
class AuthorController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'only' => ['update', 'create', 'delete', 'subscribe'],
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Author models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AuthorSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Author model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Author model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Author();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Author model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Author model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Author model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Author the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Author::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSubscribe($id)
    {
        $model = Author::findOne($id);
        $user = \Yii::$app->user->identity;
        if ($model) {
            if (UserAuthor::find()->where(['user_id' => $user->id, 'author_id' => $model->id])->exists()) {
                \Yii::$app->session->setFlash('error', 'Подписка уже оформлена.');
            } else {
                $user->link('authors', $model);
                \Yii::$app->session->setFlash('success', 'Подписка на автора успешно оформлена.');
            }
        } else {
            \Yii::$app->session->setFlash('error', 'Автор не найден.');
        }

        return $this->redirect(['index']);
    }

    public function actionUnsubscribe($id)
    {
        $model = Author::findOne($id);
        $user = \Yii::$app->user->identity;
        if ($model && UserAuthor::find()->where(['user_id' => $user->id, 'author_id' => $model->id])->exists()) {
            $userAuthor = UserAuthor::findOne(['user_id' => $user->id, 'author_id' => $model->id]);
            if ($userAuthor->delete()) {
                \Yii::$app->session->setFlash('success', 'Подписка отменена.');
            } else {
                \Yii::$app->session->setFlash('error', 'При отмене подписки возникла ошибка.');
            }
        } else {
            \Yii::$app->session->setFlash('error', 'Подписка не найдена.');
        }

        return $this->redirect(['index']);
    }
}

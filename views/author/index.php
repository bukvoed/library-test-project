<?php

use app\models\Author;
use app\models\UserAuthor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\AuthorSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Авторы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="author-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (!\Yii::$app->user->isGuest): ?>
        <p>
            <?= Html::a('Добавить автора', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'last_name',
            'first_name',
            'middle_name',
            [
                'class' => ActionColumn::className(),
                'template' => '{view} {subscribe} {unsubscribe} {update} {delete}',
                'buttons' => [
                    'subscribe' => function ($url, $model) {
                        return Html::a('Подписаться', $url);
                    },
                    'unsubscribe' => function ($url, $model) {
                        return Html::a('Отписаться', $url);
                    }
                ],
                'urlCreator' => function ($action, Author $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'visibleButtons' => [
                    'update' => !\Yii::$app->user->isGuest,
                    'delete' => !\Yii::$app->user->isGuest,
                    'subscribe' => function ($model, $key, $index) {
                        return !$model->userIsSubscribed();
                    },
                    'unsubscribe' => function ($model, $key, $index) {
                        return $model->userIsSubscribed();
                    },
                ],
            ],
        ],
    ]); ?>


</div>

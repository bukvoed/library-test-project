<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */

/** @var app\models\ContactForm $model */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;
use yii\captcha\Captcha;

$this->title = 'Отчёт';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-report">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Thank you for contacting us. We will respond to you as soon as possible.
        </div>

    <?php else: ?>

        <p>
            Топ-10 авторов по количеству опубликованных книг за указанный год.
        </p>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'report-form']); ?>

                <?= $form->field($model, 'year')->textInput(['autofocus' => true, 'type' => 'number']) ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'report-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
        <table class="table table-bordered">
            <tr>
                <th>№</th>
                <th>Автор</th>
                <th>Количество книг</th>
            </tr>
            <?php foreach ($data as $k => $v) : ?>
                <tr>
                    <td><?= $k + 1 ?></td>
                    <td><?= $v['last_name'] ?></td>
                    <td><?= $v['total'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>

    <?php endif; ?>
</div>

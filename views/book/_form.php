<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Author;

/** @var yii\web\View $this */
/** @var app\models\Book $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="book-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php
    // Normal select with ActiveForm & model
    echo $form->field($model, 'formAuthors')->widget(Select2::classname(), [
        'data' => Author::getListData(),
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите автора', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'year')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'isbn')->textInput(['maxlength' => true]) ?>

    <div class="row form-group">
        <div class="col-md-2">
            <img src="<?= $model->imageUrl ?>" width="100%" alt=""/>
            <?= $form->field($model, 'imageFile')->fileInput() ?>
        </div>
        <div class="col-md-10">
            <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

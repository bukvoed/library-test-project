<?php

use app\models\Book;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\BookSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (!\Yii::$app->user->isGuest): ?>
        <p>
            <?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'Image',
                'label' => 'Изображение',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->image ? '<img src="' . $model->getImageUrl() . '" alt="" width="120"/>' : '';
                },
            ],
            'title',
            [
                'label' => 'Авторы',
                'value' => function ($model) {
                    $items = [];
                    foreach ($model->authors as $author) {
                        $items[] = $author->fullName;
                    }
                    return implode(', ', $items);
                },
            ],
            'year',
            'description:ntext',
            'isbn',
            //'image',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Book $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'visibleButtons' => [
                    'update' => !\Yii::$app->user->isGuest,
                    'delete' => !\Yii::$app->user->isGuest,
                ],
            ],
        ],
    ]); ?>


</div>

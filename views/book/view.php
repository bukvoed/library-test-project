<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Book $model */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Книги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="book-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (!\Yii::$app->user->isGuest): ?>
        <p>
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Действительно удалить?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php endif ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'label' => 'Author',
                'value' => function ($model) {
                    $items = [];
                    foreach ($model->authors as $author) {
                        $items[] = $author->fullName;
                    }
                    return implode(', ', $items);
                },
            ],
            'year',
            'description:ntext',
            'isbn',
            [
                'attribute' => 'Image',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->image ? '<img src="' . $model->getImageUrl() . '" alt="" width="100%"/>' : '';
                },
            ],
        ],
    ]) ?>

</div>

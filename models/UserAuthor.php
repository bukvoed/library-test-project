<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_author".
 *
 * @property int $user_id
 * @property int $author_id
 */
class UserAuthor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_author';
    }
}

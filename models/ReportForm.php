<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ReportForm is the model behind the report form.
 */
class ReportForm extends Model
{
    public $year;
    public const LIMIT = 10;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->year = date("Y");
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['year', 'required'],
            ['year', 'integer'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'year' => 'Год',
        ];
    }

    /**
     */
    public function calculate()
    {
        $sql = "select a.*, count(ab.author_id) as total from 
(
select ab.* from author_book ab where ab.book_id in
(select b.id from book b where year=:year)
) ab left join author a on ab.author_id=a.id
group by ab.author_id LIMIT :limit
;";
        $params = [':year' => $this->year, ':limit' => self::LIMIT];
        return Yii::$app->db->createCommand($sql, $params)->queryAll();
    }
}

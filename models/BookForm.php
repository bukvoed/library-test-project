<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Book;
use yii\web\UploadedFile;

/**
 * BookForm represents the model behind the create/update form of `app\models\Book`.
 */
class BookForm extends Book
{
    public $formAuthors;

    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = [
            [['formAuthors'], 'safe'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, webp, jpeg'],
        ];
        return array_merge(parent::rules(), $rules);
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        return array_merge($labels, [
            'formAuthors' => 'Авторы',
            'imageFile' => 'Файл',
        ]);
    }

    public function afterFind()
    {
        parent::afterFind();
        $authors = [];
        foreach ($this->authors as $v) {
            $authors[] = $v->id;
        }
        $this->formAuthors = $authors;
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }

    public function handleUpload()
    {
        if ($this->imageFile && $this->upload()) {
            // file is uploaded successfully
            $this->image = $this->imageFile->name;
            $this->imageFile = null;
        }
        if ($this->save()) {
            $this->unlinkAll('authors', true);
            if ($this->formAuthors) {
                foreach ($this->formAuthors as $v) {
                    $author = Author::findOne($v);
                    if ($author) {
                        $this->link('authors', $author);
                    }
                }
            }
            return true;
        }
        return false;
    }
}
